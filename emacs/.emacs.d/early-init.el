(setq package-enable-at-startup t)

;; for debugging slow startup times
(setq use-package-verbose nil)

(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;;;; General theme code

(defun wander-emacs-theme-gsettings-dark-p ()
  "Return non-nil if gsettings (GNOME) has a dark theme."
  (string-match-p
   "dark"
   (shell-command-to-string "gsettings get org.gnome.desktop.interface color-scheme")))

(defun wander-emacs-theme-environment-dark-p ()
  "Return non-nil if environment theme is dark."
  (wander-emacs-theme-gsettings-dark-p))

(defun wander-emacs-re-enable-frame-theme (_frame)
  "Re-enable active theme, if any, upon FRAME creation.
Add this to `after-make-frame-functions' so that new frames do
not retain the generic background set by the function
`wander-emacs-avoid-initial-flash-of-light'."
  (when-let ((theme (car custom-enabled-themes)))
    (enable-theme theme)))

(defun wander-emacs-avoid-initial-flash-of-light ()
  "Avoid flash of light when starting Emacs, if needed.
New frames are instructed to call `wander-emacs-re-enable-frame-theme'."
  (when (wander-emacs-theme-environment-dark-p)
    (setq mode-line-format nil)
    (set-face-attribute 'default nil :background "#000000" :foreground "#ffffff")
    (set-face-attribute 'mode-line nil :background "#000000" :foreground "#ffffff" :box 'unspecified)
    (add-hook 'after-make-frame-functions #'wander-emacs-re-enable-frame-theme)))

(wander-emacs-avoid-initial-flash-of-light)
