;; In case this is the first time running this on a computer, we
;; need to make sure the following directories have been created.
(defconst wander/emacs-directory (concat (getenv "HOME") "/.emacs.d/"))
(defun wander/emacs-subdirectory (d) (expand-file-name d wander/emacs-directory))

(let* ((subdirs '("wander-emacs-modules" "wander-elisp" "wander-backups" "wander-snippets" "wander-ac-dict"))
       (fulldirs (mapcar (lambda (d) (wander/emacs-subdirectory d)) subdirs)))
  (dolist (dir fulldirs)
    (when (not (file-exists-p dir))
      (message "Make directory: %s" dir)
      (make-directory dir))))

;; add the custom lisp directories to Emacs' PATH
(mapc
 (lambda (string)
   (add-to-list 'load-path (locate-user-emacs-file string)))
 '("wander-emacs-modules" "wander-elisp"))

(setq make-backup-files t) ; can be set to nil if I don't want backups at all

;; save all auto-backups in a single directory
(setq backup-directory-alist
      `(("." . ,(expand-file-name
      	   (wander/emacs-subdirectory "wander-backups")))))

;; I haven't ever found these useful for my day-to-day
(setq create-lockfiles nil)

(setq custom-file (make-temp-file "emacs-custom-"))
(load custom-file nil)

;;;; Packages

(setq package-vc-register-as-project nil) ; Emacs 30

(add-hook 'package-menu-mode-hook #'hl-line-mode)

;; Also read: <https://protesilaos.com/codelog/2022-05-13-emacs-elpa-devel/>
(setq package-archives
      '(("gnu-elpa" . "https://elpa.gnu.org/packages/")
        ("gnu-elpa-devel" . "https://elpa.gnu.org/devel/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")
        ("melpa" . "https://melpa.org/packages/")))

;; Highest number gets priority (what is not mentioned has priority 0)
(setq package-archive-priorities
      '(("gnu-elpa" . 3)
        ("melpa" . 2)
        ("nongnu" . 1)))

;; I build Emacs from source, so I always get the
;; latest version of built-in packages.  However, this is a good
;; solution to set to non-nil if I ever switch to a stable release.
(setq package-install-upgrade-built-in nil)

(defvar wander-emacs-devel-packages
  '()
  "List of symbols representing packages I want to use the dev versions of.")

(setq package-pinned-packages
      `(,@(mapcar
           (lambda (package)
             (cons package "gnu-elpa-devel"))
           wander-emacs-devel-packages)))

(setq custom-safe-themes t)

(require 'wander-emacs-appearance)
(require 'wander-emacs-treesitter)
(require 'wander-emacs-minibuffer)
(require 'wander-emacs-autocomplete)
(require 'wander-emacs-org)
(require 'wander-emacs-magit)
(require 'wander-emacs-projectile)
(require 'wander-emacs-tweaks)
(require 'wander-emacs-ruby)
(require 'wander-emacs-applications)
