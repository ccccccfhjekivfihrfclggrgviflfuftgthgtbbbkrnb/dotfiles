(eval-after-load "hideshow"
  '(add-to-list 'hs-special-modes-alist
                `(ruby-ts-mode
                  ,(rx (or "def" "class" "module" "do" "{" "[")) ; Block start
                  ,(rx (or "}" "]" "end"))                  ; Block end
                  ,(rx (or "#" "=begin"))                   ; Comment start
                  ruby-forward-sexp nil)))

(use-package inf-ruby :ensure t)

(use-package rspec-mode
  :ensure t
  :config
  ; might affect all compilation buffers. Maybe figure out how to
  ; apply to rspec mode only
  (setq compilation-scroll-output t)
  ; allow debugging breakpoints (C-x C-q after breaakpoint hit)
  (add-hook 'after-init-hook 'inf-ruby-switch-setup))

;; There has got to be a less brittle way to get my shell PATH and
;; emacs's exec path to match...but I don't know it
(let* ((existing-path (getenv "PATH"))
       (add-to-path (string-join
 (mapc
  (lambda (path)
    (add-to-list 'exec-path (concat (getenv "HOME") path)))
  `(
    ,(concat (getenv "HOME") "/google-cloud-sdk/bin")
    ,(concat (getenv "HOME") "/.asdf/shims")
    ,(concat (getenv "HOME") "/.asdf/bin")
    ,(concat (getenv "HOME") "/.local/bin")
    ))
 ":"
 )))
(setenv "PATH" (concat add-to-path ":" existing-path)))

(setq compilation-hidden-output
      '(
        "^/home/butter/.asdf/installs/ruby/[[:digit:]]\.[[:digit:]]\.[[:digit:]]\.lib/ruby/gems.* warning.*$"
        "^Run options:.*[[:space:]]*"
        "^Test environment set up in [[:digit:]]?\.[[:digit:]]* seconds.*$"
        "^All examples were filtered out; .*ignoring.*[[:space:]]*"))

(provide 'wander-emacs-ruby)
