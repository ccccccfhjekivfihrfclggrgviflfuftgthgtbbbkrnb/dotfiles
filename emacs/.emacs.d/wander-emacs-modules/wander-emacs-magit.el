;;; Ediff
;;  ----------------------------------------------------------------------------
;; some ediff settings
(setq ediff-diff-options "")
(setq ediff-custom-diff-options "-u")
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-vertically)

  ;;; Magit
;;  ----------------------------------------------------------------------------

(use-package magit
  :ensure t
  :defer t
  :init
  (progn
    (setq magit-section-initial-visibility-alist
          '((stashes . hide) (unpushed . hide)))))

(setq magit-status-margin
      '(nil "%Y-%m-%d %H:%M " magit-log-margin-width t 18))

(provide 'wander-emacs-magit)
