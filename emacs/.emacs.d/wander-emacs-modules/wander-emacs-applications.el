(defun wander-annotate-arxiv-category (candidate)
    "Annotate arXiv category CANDIDATE with its full name."
    (when-let* ((symbol (intern-soft candidate))
                (full-name (cdr (assoc symbol arxiv-subject-classifications))))
      (concat (propertize " " 'display '(space :align-to 30))
              (propertize full-name 'face 'marginalia-documentation))))

  (use-package arxiv-mode
    :config
    (setq arxiv-default-download-folder "~/Downloads/arXiv/")
    ;; enable visual-line for arxiv by default
    (dolist (mode '(arxiv-mode-hook
                    arxiv-abstract-mode-hook))
      (add-hook mode (lambda () (visual-line-mode 1))))

    ;; use the function above as a custom annotator
    (add-to-list 'marginalia-annotator-registry
                 '(arxiv-category wander-annotate-arxiv-category none))
(require 'marginalia)
(add-to-list 'marginalia-prompt-categories '("\\<category\\>" . arxiv-category)))

(use-package ledger-mode
  :ensure t
  :mode (("\\.dat\\'" . ledger-mode))
  :config
  (setq ledger-clear-whole-transactions t)
  (setq org-read-date-popup-calendar  nil)
  :bind (:map ledger-mode-map
              ("C-c l a" . ledger-add-transaction)
              ("C-c C-x C-d" . ledger-mode-delete-transaction)
              ("C-c C-x C-r" . ledger-mode-toggle-current-transaction)
              ("C-c C-x C-s" . ledger-mode-sort-buffer)))

(provide 'wander-emacs-applications)
