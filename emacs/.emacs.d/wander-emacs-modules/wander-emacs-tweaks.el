;; Swap the two comment commands so that it is easier to use the one I always use
(global-set-key (kbd "M-;") 'comment-line)
(global-set-key (kbd "C-x C-;") 'comment-dwim)

;; I prefer spaces over TABs
(setq-default indent-tabs-mode nil)
(setq tab-width 2)
(setq-default tab-always-indent 'complete)

;; This makes it so that I just need to type "y" rather than "yes" for
;; prompts
(fset 'yes-or-no-p 'y-or-n-p)

;; This is a little package that let's me do an region select that
;; expands outwards everytime it is invoked.
(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))

;; Set a different frame title. The default is something like
;; =emacs.org - GNU Emacs at computer-name=
;; This sets it to be =[buffer-name] - Emacs 25.1.50.1=
(setq frame-title-format '("" "[%b] - Emacs " emacs-version))

;; Remove some of the default startup messaging
(setq inhibit-startup-message t)
(setq initial-scratch-message "")

;; Remove everything from the frame's outer edges
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)

;; Enable the visibe bell
(setq visible-bell t)

;; The "fringe" is basically the padding between the edge of the window
;; and the beginning of a buffer. I decreased a little bit from the
;; default (=8px=)
(set-fringe-mode 9)

;; =ibuffer= serves the same function as the default =list-buffers=, but
;; it has colors and highlighting, better filter and sorting, etc.
;; =list-buffer= will open the list of buffers in a new window, but the
;; cursor will not be in that new window.
;; =ibuffer-other-window= opens =ibuffer= in a new window, and also
;; focuses to that window, which is almost allays the behavior I want.
(defalias 'list-buffers 'ibuffer-other-window)

;; disabled tooltips
(tooltip-mode -1)

;; Highlight current line
(global-hl-line-mode 1)

;; Enable line numbers for some modes
(dolist (mode '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

;; Override some modes which derive from the above
(dolist (mode '(org-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Sometimes I run into a command that Emacs has disabled by
;; default. Here I enable some of the ones I actually use.
(put 'narrow-to-region 'disabled nil)


;; For jumping between open buffers I really like ace-window. I gave
;; avy a try, but I go faster with ace window since I can memorize the
;; jump key. The config removes the minor mode from the mode line, sets
;; the jump keys to letters instead of numbers (the default), and makes
;; the jump key prompt big and red.
;; Enable vertico
(use-package ace-window
  :ensure t
  :diminish ace-window-mode
  :init
  (setq aw-keys '(?a ?s ?d ?f ?j ?k ?l ?o))
  (custom-set-faces '(aw-leading-char-face ((t (:foreground "red" :height 3.0)))))
  :bind
  ("C-x o" . ace-window))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

(use-package emacs
  :config
  (setq package-hidden-regexps
        ; I use company-mode, so no need to see ac-* packages
        '("\\`ac-[[:alnum:]-]+[:blank:]?\\'"))
  ;; To toggle this, type M-x package-menu-toggle-hiding.
  (setq package-menu--hide-packages t))

(provide 'wander-emacs-tweaks)
