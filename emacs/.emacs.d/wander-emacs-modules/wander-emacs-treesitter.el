(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (css "https://github.com/tree-sitter/tree-sitter-css")
        (ruby "https://github.com/tree-sitter/tree-sitter-ruby")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (ledger "https://github.com/cbarrete/tree-sitter-ledger")
        (html "https://github.com/tree-sitter/tree-sitter-html")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (toml "https://github.com/tree-sitter/tree-sitter-toml")))

(setq major-mode-remap-alist
 '((ruby-mode . ruby-ts-mode)))

(provide 'wander-emacs-treesitter)
