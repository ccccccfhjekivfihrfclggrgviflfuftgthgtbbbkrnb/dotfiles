(set-face-attribute 'default nil
                    :family "FiraCode"
                    :height 140
                    :weight 'regular)
(set-face-attribute 'variable-pitch nil :family "Iosevka Etoile" :weight 'light :height 140)
(set-face-attribute 'fixed-pitch nil :family "FiraCode" :height 140 :weight 'light)

(provide 'wander-emacs-typography)
