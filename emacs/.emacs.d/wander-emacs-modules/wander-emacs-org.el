;;; Org Mode Settings
;;  ----------------------------------------------------------------------------
;; fancy bullet points in org mode
(use-package org-bullets
  :ensure t
  :after org
  :hook
  (org-mode . org-bullets-mode)
  (org-mode . org-indent-mode)
  :config
  (custom-set-faces
   '(org-level-1 ((t (:inherit ef-themes-heading-1 :extend nil :weight black :family "Inter"))))
   '(org-level-2 ((t (:inherit ef-themes-heading-2 :extend nil :slant italic :weight black :family "Inter"))))
   )

  (setq org-bullets-bullet-list '("◉" "⨀" "⦿" "⊙" "⦾" "⊚" "⌾" "○"))
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; defer running this code block unti org-mode is loaded
(with-eval-after-load 'org
  ;; bring back tab-complete templates using <s or <n
  (require 'org-tempo)

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (shell . t)
     (js . t)
     (org . t)
     (latex . t )
     )))

(provide 'wander-emacs-org)
